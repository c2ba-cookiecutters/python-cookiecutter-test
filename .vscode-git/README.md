# Sharing VSCode workspaces between developpers

Put here you VSCode workspace in a folder with your name and create a symbolic
link to it at the root of your local repository:

```
python .vscode-git/link.py <config-name> # <config-name> defaults to "default"
```
