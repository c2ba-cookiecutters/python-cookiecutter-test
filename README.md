# Test Project

## Introduction



## Developer setup

You need [poetry](https://python-poetry.org/) installed on your system and accessible from your PATH.

After cloning the repository, you can setup a developer virtual environment by running the boostrap script:

```bash
bash bootstrap.sh
```

Then you can use poetry to run various dev tools:

```bash
poetry run pytest
poetry run flake8
```

You can run continuous integration scripts using nox:

```bash
poetry run nox -rs
```
