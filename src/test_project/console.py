"""Console script for test-project."""

import typer

from test_project import (
    __version__,
)

app = typer.Typer()


def version_callback(value: bool) -> None:
    if value:
        typer.echo(f"Test Project version {__version__}")
        raise typer.Exit()


@app.command()
def main(
    version: bool = typer.Option(False, "--version", callback=version_callback),
) -> None:
    """Console script for test-project."""


if __name__ == "__main__":  # pragma: no cover
    app()
