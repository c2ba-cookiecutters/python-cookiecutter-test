import pytest
from typer.testing import (
    CliRunner,
    Result,
)

from test_project import (
    __version__,
    console,
)


@pytest.fixture()
def runner() -> CliRunner:
    return CliRunner()


@pytest.fixture()
def app_result(runner: CliRunner) -> Result:
    return runner.invoke(console.app)


def test_app_succeeds(app_result: Result) -> None:
    assert app_result.exit_code == 0


def test_version(runner: CliRunner) -> None:
    result = runner.invoke(console.app, ["--version"])
    assert result.exit_code == 0
    assert __version__ in result.stdout
